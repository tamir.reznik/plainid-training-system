CREATE TABLE users (
    user_id VARCHAR(40) NOT NULL,
	user_name VARCHAR(60) NOT NULL,
	department VARCHAR(60) NOT NULL,
	country VARCHAR(60),
	on_create TIMESTAMP NOT NULL,
	UNIQUE(user_name, department, country),
    PRIMARY KEY(user_id, department, country)
);

CREATE TABLE groups (
    group_id VARCHAR(40) NOT NULL,
    group_name VARCHAR(60) NOT NULL,
    attributes JSONB NOT NULL,
    constraint group_pk PRIMARY KEY(group_id)
);

CREATE TABLE apps (
    app_id VARCHAR(40) NOT NULL,
    app_name VARCHAR(60) NOT NULL,
    PRIMARY KEY(app_id)
);

CREATE TABLE assets (
    asset_id VARCHAR(40) NOT NULL,
    asset_name VARCHAR(60) NOT NULL,
    PRIMARY KEY(asset_id)
);

CREATE TABLE conditions (
    condition_id VARCHAR(40) NOT NULL,
    condition_name VARCHAR(60) NOT NULL,
    rule JSONB NOT NULL,
    PRIMARY KEY(condition_id)
);

CREATE TABLE policies (
    policy_id VARCHAR(40) NOT NULL,
    policy_name VARCHAR(60) NOT NULL,
    PRIMARY KEY(policy_id)
);

CREATE TABLE policies_conditions(
policy_id VARCHAR(40) NOT NULL,
condition_id VARCHAR(40) NOT NULL,
FOREIGN KEY(policy_id) REFERENCES policies(policy_id),
FOREIGN KEY(condition_id) REFERENCES conditions(condition_id)
);

CREATE TABLE policies_assets(
policy_id VARCHAR(40) NOT NULL,
asset_id VARCHAR(40) NOT NULL,
FOREIGN KEY(policy_id) REFERENCES policies(policy_id),
FOREIGN KEY(asset_id) REFERENCES assets(asset_id)
);

CREATE TABLE policies_groups(
policy_id VARCHAR(40) NOT NULL,
group_id VARCHAR(40) NOT NULL,
FOREIGN KEY(policy_id) REFERENCES policies(policy_id),
FOREIGN KEY(group_id) REFERENCES groups(group_id)
);

CREATE TABLE policies_apps(
policy_id VARCHAR(40) NOT NULL,
app_id VARCHAR(40) NOT NULL,
FOREIGN KEY(policy_id) REFERENCES policies(policy_id),
FOREIGN KEY(app_id) REFERENCES apps(app_id)
);









