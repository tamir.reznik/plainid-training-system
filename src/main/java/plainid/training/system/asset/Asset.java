package plainid.training.system.asset;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "assets")
public class Asset {

    @Id
    private String asset_id;
    private String asset_name;

    public Asset() {
    }

    public Asset(String asset_id, String asset_name) {
        this.asset_id = asset_id;
        this.asset_name = asset_name;
    }

    public String getAsset_id() {
        return asset_id;
    }

    public void setAsset_id(String asset_id) {
        this.asset_id = asset_id;
    }

    public String getAsset_name() {
        return asset_name;
    }

    public void setAsset_name(String asset_name) {
        this.asset_name = asset_name;
    }
}
