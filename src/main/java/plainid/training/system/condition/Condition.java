package plainid.training.system.condition;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "conditions")
public class Condition {

    @Id
    private String condition_id;
    private String condition_name;
    //jsonB rule;


    public Condition() {
    }

    public Condition(String condition_id, String condition_name) {
        this.condition_id = condition_id;
        this.condition_name = condition_name;
    }

    public String getCondition_id() {
        return condition_id;
    }

    public void setCondition_id(String condition_id) {
        this.condition_id = condition_id;
    }

    public String getCondition_name() {
        return condition_name;
    }

    public void setCondition_name(String condition_name) {
        this.condition_name = condition_name;
    }
}

